# mastodon-watch-followers

## Usage

```
go get -u gitlab.com/Strubbl/mastodon-watch-followers
mastodon-watch-followers -h
```

If you have no config yet, you can create a dummy config with the following command:
```
mastodon-watch-followers -dummy
```

module codeberg.org/strubbl/mastodon-watch-followers

go 1.22

require github.com/mattn/go-mastodon v0.0.9

require (
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80 // indirect
)
